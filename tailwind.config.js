module.exports = {
  darkMode: "class",
  purge: ["./src/**/*.js"],
  variants: {},
  plugins: [],
  theme: {
    extend: {
      height: theme => ({
        '600px':'600px',
        '200px':'200px',
        '30px':'30px',
        '40px':'40px',
        '60px':'60px'
      }),
      width: theme => ({
        '100px':'100px',
      }),
      margin: theme => ({
        '1px':'1px',
      })
    }
  }
};
