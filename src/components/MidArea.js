import React, {useState, useRef, useEffect} from "react";
import EventBlock from './EventBlock';
import MotionBlock from './MotionBlock';
import ControlBlock from './ControlBlock';
import LookBlock from './LookBlock';
import motionFunctionality from '../utility/motionFunctionality';
import eventFunctionality from '../utility/eventFunctionality';
import controlFunctionality from '../utility/controlFunctionality';
import lookFunctionality from '../utility/lookFunctionality';
import BlockNode from '../utility/BlockNode';
import _ from 'lodash';

export default function MidArea({getSpriteState, updateSpriteState,spriteId ,executables,setExecutables, compiler, spriteExecutions}) {

  const parentRef   = useRef(null);
  const [parentHeight,setParentHeight] = useState(0);
  const [parentWidth,setParentWidth] = useState(0);
  const [parentY,setParentY] = useState(0);
  const [parentX,setParentX] = useState(0);

  const getBlockStyle = (block, index) => {

    const blockPosition = block.position;
    const xPosition = blockPosition.x - parentX;
    const yPosition = blockPosition.y - parentY;
    return { transform:`translate(${xPosition}px,${yPosition}px)`}
  }

  const sendData = (event,executableId, nodeId) => {
    event.dataTransfer.setData("executableId",executableId);
    event.dataTransfer.setData("nodeId",nodeId);
    event.dataTransfer.setData("origin",'midarea');
    event.stopPropagation();
  }

  const setSpriteExecutablesToState = (spriteExecutables) => {
    const oldState = [...executables];
    oldState[spriteId] = spriteExecutables;
    setExecutables(oldState);

  }

  const updateDynamicArray = (event,dynamicIndex,executableId,nodeId) => {
    const oldSpriteExecutables = executables[spriteId];
    const oldExecutableChain = getExecutableChain(oldSpriteExecutables,executableId);
    const newNode = getNode(oldExecutableChain, nodeId);
    newNode.callbackAction['dynamicArguments'][dynamicIndex] = event.target.value;
    const updatedExecutableChain = replaceNode(oldExecutableChain,newNode,nodeId);
    const newSpriteExecutable = replaceExecutableChain(oldSpriteExecutables,updatedExecutableChain,executableId);
    setSpriteExecutablesToState(newSpriteExecutable)
  }

  const addExecutableBlock = (oldSpriteExecutables,newExecutableBlock) => {

    let newSpriteExecutables = oldSpriteExecutables;
    if(newSpriteExecutables === undefined) {
      newSpriteExecutables = [];
      newSpriteExecutables.push(newExecutableBlock);
    } else {
      newSpriteExecutables.push(newExecutableBlock);
    }

    return newSpriteExecutables;
  }
  
  const getExecutableChain = (spriteExecutables,executableId) => {
      return _.cloneDeep(spriteExecutables[executableId].executableChain);
  }
  
  const replaceExecutableChain = (oldSpriteExecutables,newExecutableChain,executableId) => {

    if(newExecutableChain.length === 0){
      const newSpriteExecutables = [
        ...oldSpriteExecutables.slice(0,executableId),
        ...oldSpriteExecutables.slice(executableId+1)
      ]
      return newSpriteExecutables;
    } else {
      const newSpriteExecutables = oldSpriteExecutables;
      newSpriteExecutables[executableId].executableChain = newExecutableChain;
      return newSpriteExecutables;
    }
    
  }
  
  const addBetweenNodes = (oldExecutableChain, nodes, nodeId) => {

    if(oldExecutableChain === undefined) {
      const newExecutableChain = [];
      newExecutableChain.push(nodes);
      return _.cloneDeep(newExecutableChain);
    } else {
      const newExecutableChain = [

        ...oldExecutableChain.slice(0,nodeId+1),
        ...nodes,
        ...oldExecutableChain.slice(nodeId+1)

      ]
      return _.cloneDeep(newExecutableChain);

    }
    

  }

  const getNode = (oldExecutableChain, nodeId) => {
      return _.cloneDeep(oldExecutableChain[nodeId]);
  }

  const getNodesTillEnd = (oldExecutableChain, nodeId) => {
    return _.cloneDeep(oldExecutableChain.slice(nodeId));
  }
  
  const replaceNode = (oldExecutableChain, node,nodeId) => {

      const newExecutableChain = [

        ...oldExecutableChain.slice(0,nodeId),
        node,
        ...oldExecutableChain.slice(nodeId+1)

      ]
      return _.cloneDeep(newExecutableChain);
  
  }

  const removeNode = (executableId, nodeId) => {
  
      const updatedExecutableChain = [
  
          ...executables[spriteId][executableId].slice(0,nodeId),
          ...executables[spriteId][executableId].slice(nodeId+1),
  
      ]
      executables[spriteId][executableId] = updatedExecutableChain;
  
  }

  const removeNodesTillEnd = (oldExecutableChain, nodeId) => {
  
    const newExecutableChain = [
        ...oldExecutableChain.slice(0,nodeId),
    ]
    return _.cloneDeep(newExecutableChain);

  }

  const dragOver = (event) => {
    event.preventDefault();
  }

  const createNewChain = (event) => {

    const origin = event.dataTransfer.getData("origin");
    const executableBlock = {position:{x:event.clientX,y:event.clientY}};
    if(origin === 'midarea') {
      const previousExecId = parseInt(event.dataTransfer.getData("executableId"));
      const previousNodeId = parseInt(event.dataTransfer.getData("nodeId"));
      const oldSpriteExecutables = executables[spriteId];
      const oldExecutableChain = getExecutableChain(oldSpriteExecutables, previousExecId); 
      const newExecutableChain  = getNodesTillEnd(oldExecutableChain, previousNodeId);
      
      const dragRemovedExecutableChain = removeNodesTillEnd(oldExecutableChain, previousNodeId);
      const dragRemovedSpriteExecutable = replaceExecutableChain(oldSpriteExecutables,dragRemovedExecutableChain,previousExecId);
      executableBlock.executableChain = newExecutableChain;
      const finalSpriteExecutable = addExecutableBlock(dragRemovedSpriteExecutable,executableBlock);
      setSpriteExecutablesToState(finalSpriteExecutable);

    } else {
      const oldSpriteExecutables = executables[spriteId];
      const newExecutableChain = [];
      const contentIndex = event.dataTransfer.getData("contentIndex");
      const blockType = event.dataTransfer.getData("blockType");
      switch(blockType) {
        case 'motion':
          newExecutableChain.push(
              new BlockNode(
                blockType,
                motionFunctionality[contentIndex].canHaveHead,
                motionFunctionality[contentIndex].canHaveTail,
              _.cloneDeep(motionFunctionality[contentIndex].callbackAction),
              _.cloneDeep(motionFunctionality[contentIndex].contentArray),
                motionFunctionality[contentIndex].eventId
              )
          )
          break;
        case 'event':
          newExecutableChain.push(
            new BlockNode(
              blockType,
              eventFunctionality[contentIndex].canHaveHead,
              eventFunctionality[contentIndex].canHaveTail,
              _.cloneDeep(eventFunctionality[contentIndex].callbackAction),
              _.cloneDeep(eventFunctionality[contentIndex].contentArray),
              eventFunctionality[contentIndex].eventId
            )
          )
          break;
          case 'control':
            newExecutableChain.push(
              new BlockNode(
                blockType,
                controlFunctionality[contentIndex].canHaveHead,
                controlFunctionality[contentIndex].canHaveTail,
                _.cloneDeep(controlFunctionality[contentIndex].callbackAction),
                _.cloneDeep(controlFunctionality[contentIndex].contentArray),
                controlFunctionality[contentIndex].eventId,
              )
            )
            break;
          case 'look':
            newExecutableChain.push(
              new BlockNode(
                blockType,
                lookFunctionality[contentIndex].canHaveHead,
                lookFunctionality[contentIndex].canHaveTail,
                _.cloneDeep(lookFunctionality[contentIndex].callbackAction),
                _.cloneDeep(lookFunctionality[contentIndex].contentArray),
                lookFunctionality[contentIndex].eventId,
              )
            )
            break;
      }
      executableBlock.executableChain = newExecutableChain;
      const newSpriteExecutables = addExecutableBlock(oldSpriteExecutables,executableBlock);
      setSpriteExecutablesToState(newSpriteExecutables);
    }
    

  }

  const addToChain = (event,executableId,nodeId) => {
    
    const origin = event.dataTransfer.getData("origin");
    const oldSpriteExecutables = executables[spriteId];
    if(origin === 'midarea') {
      const previousExecId = parseInt(event.dataTransfer.getData("executableId"));
      const previousNodeId = parseInt(event.dataTransfer.getData("nodeId"));
      const oldSpriteExecutableLength = oldSpriteExecutables.length;

      const oldExecutableChain = getExecutableChain(oldSpriteExecutables, previousExecId);
      const draggedNodes = getNodesTillEnd(oldExecutableChain, previousNodeId);

      if(!draggedNodes[0].canHaveHead) return;

      const dragRemovedExecutableChain = removeNodesTillEnd(oldExecutableChain, previousNodeId);
      const dragRemovedSpriteExecutable = replaceExecutableChain(oldSpriteExecutables,dragRemovedExecutableChain,previousExecId);
      const newExecutableId = (oldSpriteExecutableLength>dragRemovedSpriteExecutable.length)&&(previousExecId<executableId)? executableId-1:executableId;
      const finalExecutableId = newExecutableId < 0? 0 : newExecutableId;

      const draggedToExecutableChain = getExecutableChain(dragRemovedSpriteExecutable, finalExecutableId);
      const dragNodesAddedExecutableChain = addBetweenNodes(draggedToExecutableChain,draggedNodes,nodeId);
      const dragAddedSpriteExecutables = replaceExecutableChain(dragRemovedSpriteExecutable, dragNodesAddedExecutableChain, finalExecutableId);
      setSpriteExecutablesToState(dragAddedSpriteExecutables);
      event.stopPropagation();
      return dragAddedSpriteExecutables;
    } else {
      const contentIndex = event.dataTransfer.getData("contentIndex");
      const blockType = event.dataTransfer.getData("blockType");
      const oldExecutableChain = getExecutableChain(executables[spriteId],executableId);
      let newBlock;
      switch(blockType) {
        case 'motion':
          if(!motionFunctionality[contentIndex].canHaveHead) return;
          newBlock = new BlockNode(
            blockType,
            motionFunctionality[contentIndex].canHaveHead,
            motionFunctionality[contentIndex].canHaveTail,
            _.cloneDeep(motionFunctionality[contentIndex].callbackAction),
            _.cloneDeep(motionFunctionality[contentIndex].contentArray),
            motionFunctionality[contentIndex].eventId
          )
          break;
        case 'event':
          if(!eventFunctionality[contentIndex].canHaveHead) return;
          newBlock = new BlockNode(
            blockType,
            eventFunctionality[contentIndex].canHaveHead,
            eventFunctionality[contentIndex].canHaveTail,
            _.cloneDeep(eventFunctionality[contentIndex].callbackAction),
            _.cloneDeep(eventFunctionality[contentIndex].contentArray),
            eventFunctionality[contentIndex].eventId
          )
          break;
        case 'control':
          if(!controlFunctionality[contentIndex].canHaveHead) return;
          newBlock = new BlockNode(
            blockType,
            controlFunctionality[contentIndex].canHaveHead,
            controlFunctionality[contentIndex].canHaveTail,
            _.cloneDeep(controlFunctionality[contentIndex].callbackAction),
            _.cloneDeep(controlFunctionality[contentIndex].contentArray),
            controlFunctionality[contentIndex].eventId,
          )
          break;
        case 'look':
          if(!lookFunctionality[contentIndex].canHaveHead) return;
          newBlock = new BlockNode(
            blockType,
            lookFunctionality[contentIndex].canHaveHead,
            lookFunctionality[contentIndex].canHaveTail,
            _.cloneDeep(lookFunctionality[contentIndex].callbackAction),
            _.cloneDeep(lookFunctionality[contentIndex].contentArray),
            lookFunctionality[contentIndex].eventId,
          )
          break;
      }
      const newExecutableChain = addBetweenNodes(oldExecutableChain,[newBlock],nodeId);
      const newSpriteExecutables = replaceExecutableChain(oldSpriteExecutables,newExecutableChain,executableId);
      setSpriteExecutablesToState(newSpriteExecutables);
      event.stopPropagation();
      return newSpriteExecutables;
    }
    
  }

  const addToHead = (event, executableId) => {

    const origin = event.dataTransfer.getData("origin");
    
    const oldSpriteExecutables = executables[spriteId];
    if(origin === 'midarea') {
      const previousExecId = parseInt(event.dataTransfer.getData("executableId"));
      const previousNodeId = parseInt(event.dataTransfer.getData("nodeId"));
      const oldSpriteExecutableLength = oldSpriteExecutables.length;

      const oldExecutableChain = getExecutableChain(oldSpriteExecutables, previousExecId);
      const draggedNodes = getNodesTillEnd(oldExecutableChain, previousNodeId);

      const dragRemovedExecutableChain = removeNodesTillEnd(oldExecutableChain, previousNodeId);
      const dragRemovedSpriteExecutable = replaceExecutableChain(oldSpriteExecutables,dragRemovedExecutableChain,previousExecId);
      const newExecutableId = (oldSpriteExecutableLength>dragRemovedSpriteExecutable.length)&&(previousExecId<executableId)? executableId-1:executableId;
      const finalExecutableId = newExecutableId < 0? 0 : newExecutableId;
      const draggedToExecutableChain = getExecutableChain(dragRemovedSpriteExecutable, finalExecutableId);
      const dragNodesAddedExecutableChain = addBetweenNodes(draggedToExecutableChain,draggedNodes,-1);
      const dragAddedSpriteExecutables = replaceExecutableChain(dragRemovedSpriteExecutable, dragNodesAddedExecutableChain, finalExecutableId);

      setSpriteExecutablesToState(dragAddedSpriteExecutables);

    } else {
      const contentIndex = event.dataTransfer.getData("contentIndex");
      const blockType = event.dataTransfer.getData("blockType");
      const oldExecutableChain = getExecutableChain(executables[spriteId],executableId);
      let newBlock;
      switch(blockType) {
        case 'motion':
          newBlock = new BlockNode(
            blockType,
            motionFunctionality[contentIndex].canHaveHead,
            motionFunctionality[contentIndex].canHaveTail,
            _.cloneDeep(motionFunctionality[contentIndex].callbackAction),
            _.cloneDeep(motionFunctionality[contentIndex].contentArray),
            motionFunctionality[contentIndex].eventId,
          )
          break;
        case 'event':
          newBlock = new BlockNode(
            blockType,
            eventFunctionality[contentIndex].canHaveHead,
            eventFunctionality[contentIndex].canHaveTail,
            _.cloneDeep(eventFunctionality[contentIndex].callbackAction),
            _.cloneDeep(eventFunctionality[contentIndex].contentArray),
            eventFunctionality[contentIndex].eventId,
          )
          break;
        case 'control':
          newBlock = new BlockNode(
            blockType,
            controlFunctionality[contentIndex].canHaveHead,
            controlFunctionality[contentIndex].canHaveTail,
            _.cloneDeep(controlFunctionality[contentIndex].callbackAction),
            _.cloneDeep(controlFunctionality[contentIndex].contentArray),
            controlFunctionality[contentIndex].eventId,
          )
          break;
        case 'look':
          newBlock = new BlockNode(
            blockType,
            lookFunctionality[contentIndex].canHaveHead,
            lookFunctionality[contentIndex].canHaveTail,
            _.cloneDeep(lookFunctionality[contentIndex].callbackAction),
            _.cloneDeep(lookFunctionality[contentIndex].contentArray),
            lookFunctionality[contentIndex].eventId,
          )
          break;
      }
      const newExecutableChain = addBetweenNodes(oldExecutableChain,[newBlock],-1);
      const newSpriteExecutables = replaceExecutableChain(oldSpriteExecutables,newExecutableChain,executableId);

      setSpriteExecutablesToState(newSpriteExecutables);
    }
    event.stopPropagation();
  }

  const deleteNode = (event,executableId,nodeId) => {
    if(event.key === 'Delete'){
      const oldSpriteExecutables = executables[spriteId];
      const oldExecutableChain = getExecutableChain(oldSpriteExecutables,executableId);
      const updatedExecutableChain = removeNodesTillEnd(oldExecutableChain, nodeId);
      const newSpriteExecutables = replaceExecutableChain(oldSpriteExecutables,updatedExecutableChain,executableId);
      setSpriteExecutablesToState(newSpriteExecutables);
    }
    event.stopPropagation();
  }

  const addToControlChain = (event,executableId, nodeId, layerIndex) => {
    event.preventDefault();
    const origin = event.dataTransfer.getData("origin");
    let count = 1;
    if(origin === 'midarea') {
      const previousExecId = parseInt(event.dataTransfer.getData("executableId"));
      const previousNodeId = parseInt(event.dataTransfer.getData("nodeId"));
      const oldExecutableChain = getExecutableChain(oldSpriteExecutables, previousExecId);
      const draggedNodes = getNodesTillEnd(oldExecutableChain, previousNodeId);
      count = draggedNodes.length;
    }
    const chainAddedExecutables = addToChain(event,executableId,nodeId);
    const oldSpriteExecutables = executables[spriteId];
    const oldExecutableChain = getExecutableChain(oldSpriteExecutables,executableId);
    const newNode = getNode(oldExecutableChain, nodeId);
    newNode.contentArray['innerValues'][layerIndex] = newNode.contentArray['innerValues'][layerIndex]+count;
    const updatedExecutableChain = replaceNode(chainAddedExecutables[executableId].executableChain, newNode,nodeId);
    const newSpriteExecutables = replaceExecutableChain(oldSpriteExecutables,updatedExecutableChain,executableId);
    setSpriteExecutablesToState(newSpriteExecutables);
    event.stopPropagation();
  }

  const draggedOnTop = (event) => {
  }

  const draggedOnExit = (event) => {
  }

  const renderThroughList = (executableBlock,executableId,nodeId)=> {
    
    if(executableBlock.executableChain.length <= nodeId) {
      return ;
    }
    const blockNode = executableBlock.executableChain[nodeId];
    return (
      <>
        <div key={executableId+''+nodeId} tabIndex="0" onKeyDown={(event) =>deleteNode(event,executableId,nodeId)} draggable={true} onDragStart={(event) =>sendData(event,executableId,nodeId)} onDragOver={dragOver}  onDrop={(event) => addToChain(event,executableId, nodeId)} >
          {
            blockNode.blockType === 'motion' &&
            <MotionBlock
            callbackAction={blockNode['callbackAction']}
            contentArray={blockNode['contentArray']}
            currentSelectedSprite={spriteId}
            getSpriteState={getSpriteState}
            updateSpriteState={updateSpriteState}
            updateDynamicArguments={updateDynamicArray}
            nodeId={nodeId}
            executableId={executableId}
            origin='midarea'
            />
          } {
            blockNode.blockType === 'event' &&
            <EventBlock
            callbackAction={blockNode['callbackAction']}
            contentArray={blockNode['contentArray']}
            currentSelectedSprite={spriteId}
            getSpriteState={getSpriteState}
            updateSpriteState={updateSpriteState}
            updateDynamicArguments={updateDynamicArray}
            nodeId={nodeId}
            executableId={executableId}
            origin='midarea'
            /> 
          } {
            blockNode.blockType === 'control' &&

            <ControlBlock
            callbackAction={blockNode['callbackAction']}
            contentArray={blockNode['contentArray']}
            currentSelectedSprite={spriteId}
            getSpriteState={getSpriteState}
            updateSpriteState={updateSpriteState}
            updateDynamicArguments={updateDynamicArray}
            nodeId={nodeId}
            executableId={executableId}
            addToControlChain={addToControlChain}
            origin='midarea'
            />
          } {
            blockNode.blockType === 'look' &&
            <LookBlock
            callbackAction={blockNode['callbackAction']}
            contentArray={blockNode['contentArray']}
            currentSelectedSprite={spriteId}
            getSpriteState={getSpriteState}
            updateSpriteState={updateSpriteState}
            updateDynamicArguments={updateDynamicArray}
            nodeId={nodeId}
            executableId={executableId}
            origin='midarea'
            /> 
          }
          {blockNode['canHaveTail'] &&
          (<div>
            {renderThroughList(executableBlock,executableId,nodeId+1)}
          </div>)}
        </div>
        
      </>)


  }

  useEffect ( () => {
          
    if(parentRef.current){
        
        const parentY = parentRef.current.offsetTop;
        const parentX = parentRef.current.offsetLeft;
        setParentY(parentY);
        setParentX(parentX); 
    }

    
  }, [parentRef]);

  useEffect ( () => {
    compiler(spriteId); 
    
  }, [executables]);

  return (
    <div onDragOver={dragOver} onDrop={createNewChain}  className="flex-1 h-full overflow-auto static" ref={parentRef}> 
    {
      (spriteId in executables) &&
      executables[spriteId].map((executableBlock,executableId) => (

        <div onClick={() =>{spriteExecutions[spriteId][executableId]()}} key={executableId} className='w-max absolute' style={getBlockStyle(executableBlock)}>
          {executableBlock.executableChain[0]['canHaveHead'] &&
            (<div onDragOver={dragOver} onDrop={(event) => addToHead(event,executableId)} className="hasHead h-4"> </div>)}
          {
            renderThroughList(executableBlock,executableId,0)
          }
        </div>
      ))

    }
    </div>
  );
}