import React from "react";
import contentHandler from '../utility/contentHandler';

export default function EventBlock({ contentArray, callbackAction,getSpriteState,updateSpriteState,currentSelectedSprite, updateDynamicArguments, executableId, nodeId, origin, contentIndex }) {

  const classnameCustom = origin === 'sidebar'? 'rounded':'rounded-t'
  const onClickSideBar = () =>{callbackAction['action'](...callbackAction.dynamicArguments,...callbackAction.staticArguments,currentSelectedSprite ,getSpriteState,updateSpriteState)}
  const onClickMidarea = () =>{}
  return (
    (
    <div onClick={origin==='sidebar'?onClickSideBar:onClickMidarea} className={`flex flex-row flex-wrap w-max bg-yellow-500 text-white px-2 py-1 text-sm cursor-pointer ${classnameCustom}`}>
        {
          contentArray.map((contentItem,index) => (
            <div key={contentItem.type+index}>
              {
                contentHandler[contentItem.type]
                (
                  ...contentItem.arguments,
                  (event) => 
                    {
                      origin === 'sidebar'?
                      updateDynamicArguments(event, contentItem.index, contentIndex,'event'):
                      updateDynamicArguments(event,contentItem.index,executableId,nodeId)
                    },
                  callbackAction.dynamicArguments[contentItem.index],
                  contentItem.type+index
                )
              }  
            </div>
          ))
        }
    </div>)
  );
}
