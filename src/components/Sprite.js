import React, {useRef, useEffect, useState} from "react";
import CatSprite from "./CatSprite";
import {Tooltip} from 'react-tippy';
import Icon from "./Icon";

//{x:0,y:0,angle:0,height:100,width:100}

export default function Sprite({index,sprite ,handleDrag, setCurrentSprite, getSpriteStyle,onSpriteClick, updateDraggable}) {

  const [isDraggable, setIsDraggable] = useState(false);

  const mouseUp = async(value) => {

    await onSpriteClick(index);
    updateDraggable(value,index);

  }

  const dragWithMouse = (event) => {
    if(sprite.isDraggable && event.buttons === 1 ){
      
      handleDrag(event,index,()=>{setIsDraggable(false)});
    }
  }
  const setDraggable = (value) => {
    updateDraggable(value,index);
  }
  return (
    <>
    {
      !sprite.hide &&
    <div onMouseMove={(event) =>{dragWithMouse(event)}} onMouseDown={() =>setDraggable(true)} onMouseUp={() =>{mouseUp(false)}} onDoubleClick={() => setCurrentSprite(index)} className='relative z-10' style={getSpriteStyle(sprite,index)}>
    <Tooltip
      title={sprite.interactionContent} 
      disabled={(sprite.interactionContent === '')}
      open={true}
      position="top-end"
      distance={-100}
      offset={-100}
      trigger="manual"
      theme='dark'
      arrow={true}
      size="big"
      distance="25"
    >
    <CatSprite height={sprite.height} width={sprite.width} />
    </Tooltip>
    </div>
    }
    </>
  );
}