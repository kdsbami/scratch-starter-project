import React,{useState} from "react";
import EventBlock from './EventBlock';
import MotionBlock from './MotionBlock';
import ControlBlock from './ControlBlock';
import LookBlock from './LookBlock';
import motionFunctionality from '../utility/motionFunctionality';
import eventFunctionality from '../utility/eventFunctionality';
import controlFunctionality from '../utility/controlFunctionality';
import lookFunctionality from '../utility/lookFunctionality';

export default function Sidebar({getSpriteState, updateSpriteState,currentSelectedSprite}) {
  const [motionNodes, setMotionNodes] = useState(motionFunctionality);
  const [eventNodes, setEventNodes] = useState(eventFunctionality);
  const [controlNodes, setControlNodes] = useState(controlFunctionality);
  const [lookNodes, setLookNodes] = useState(lookFunctionality);
  
  const sendData = (event,index,blockType) => {

    event.dataTransfer.setData("contentIndex",index);
    event.dataTransfer.setData("origin",'sidebar');
    event.dataTransfer.setData("blockType",blockType);
 
  }

  const updateDynamicArray = (event, dynamicIndex, contentIndex, contentType) => {
    event.preventDefault();
    if(contentType === 'event') {
      const newNodes = eventNodes;
      const newNode = eventNodes[contentIndex];
      newNode.callbackAction['dynamicArguments'][dynamicIndex] = event.target.value;
      newNodes[contentIndex] = newNode;
      setEventNodes([...newNodes]);
    }
    else if(contentType === 'motion') {
      const newNodes = motionNodes;
      const newNode = motionNodes[contentIndex];
      newNode.callbackAction['dynamicArguments'][dynamicIndex] = event.target.value;
      newNodes[contentIndex] = newNode;
      setMotionNodes([...newNodes]);
    }
    else if(contentType === 'control') {
      const newNodes = controlNodes;
      const newNode = controlNodes[contentIndex];
      newNode.callbackAction['dynamicArguments'][dynamicIndex] = event.target.value;
      newNodes[contentIndex] = newNode;
      setControlNodes([...newNodes]);
    }
    else if(contentType === 'look') {
      const newNodes = lookNodes;
      const newNode = lookNodes[contentIndex];
      newNode.callbackAction['dynamicArguments'][dynamicIndex] = event.target.value;
      newNodes[contentIndex] = newNode;
      setLookNodes([...newNodes]);
    }
  }

  return (
    <div className="w-60 flex-none h-full overflow-y-auto flex flex-col items-start p-2 border-r border-gray-200">
      <div className="font-bold"> {"Events"} </div>
      {
        eventNodes.map((block,index) => (
        <div className="m-1px" key={index+block.contentArray} draggable={true} onDragStart={(event) => sendData(event,index,'event')} >
          <EventBlock 
            callbackAction={block.callbackAction} 
            contentArray={block.contentArray}
            currentSelectedSprite={currentSelectedSprite} 
            getSpriteState={getSpriteState} 
            updateSpriteState={updateSpriteState}
            updateDynamicArguments={updateDynamicArray}
            contentIndex={index}
            origin='sidebar'
          />
      </div>
        ))
      }
      <div className="font-bold"> {"Motion"} </div>
      {
        motionNodes.map((block,index) => (
        <div className="m-1px" key={index+block.contentArray} draggable={true} onDragStart={(event) => sendData(event,index,'motion')} >
          <MotionBlock 
            callbackAction={block.callbackAction} 
            contentArray={block.contentArray}
            currentSelectedSprite={currentSelectedSprite} 
            getSpriteState={getSpriteState} 
            updateSpriteState={updateSpriteState}
            updateDynamicArguments={updateDynamicArray}
            contentIndex={index}
            origin='sidebar'
          />
      </div>
        ))
      }
       <div className="font-bold"> {"Control"} </div>
      {
        controlNodes.map((block,index) => (
        <div className="m-1px" key={index+block.contentArray} draggable={true} onDragStart={(event) => sendData(event,index,'control')} >
          <ControlBlock 
            callbackAction={block.callbackAction} 
            contentArray={block.contentArray}
            currentSelectedSprite={currentSelectedSprite} 
            getSpriteState={getSpriteState} 
            updateSpriteState={updateSpriteState}
            updateDynamicArguments={updateDynamicArray}
            contentIndex={index}
            origin='sidebar'
          />
      </div>
        ))
      }
       <div className="font-bold"> {"Look"} </div>
      {
        lookNodes.map((block,index) => (
        <div className="m-1px" key={index+block.contentArray} draggable={true} onDragStart={(event) => sendData(event,index,'look')} >
          <LookBlock 
            callbackAction={block.callbackAction} 
            contentArray={block.contentArray}
            currentSelectedSprite={currentSelectedSprite} 
            getSpriteState={getSpriteState} 
            updateSpriteState={updateSpriteState}
            updateDynamicArguments={updateDynamicArray}
            contentIndex={index}
            origin='sidebar'
          />
      </div>
        ))
      }
    </div>
  );
}
