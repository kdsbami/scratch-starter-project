import React from "react";
import contentHandler from '../utility/contentHandler';

export default function LookBlock({ contentArray, callbackAction,getSpriteState,updateSpriteState,currentSelectedSprite, updateDynamicArguments, executableId, nodeId, origin, contentIndex }) {

  const classnameCustom = origin === 'sidebar'? 'rounded':'';
  const onClickSideBar = () =>{callbackAction['action'](...callbackAction.dynamicArguments,...callbackAction.staticArguments,currentSelectedSprite ,getSpriteState,updateSpriteState)}
  const onClickMidarea = () =>{}
  return (
    
    (
    <div onClick={origin==='sidebar'?onClickSideBar:onClickMidarea} className={`flex flex-row flex-wrap w-max text-white bg-purple-600 px-2 py-1 text-sm cursor-pointer ${classnameCustom}`}>
        {
          contentArray.map((contentItem,index) => (
          <div key={contentItem.type+index}>
            {
            contentHandler[contentItem.type]
            (
            ...contentItem.arguments,
            (event) => 
              {
              origin === 'sidebar'?
              updateDynamicArguments(event, contentItem.index, contentIndex,'look'):
              updateDynamicArguments(event,contentItem.index,executableId,nodeId)
              },
            callbackAction.dynamicArguments[contentItem.index]
            )
            }
          </div>
          ))
        }
    </div>)
  );
}
