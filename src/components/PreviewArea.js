import React, {useRef, useEffect, useState} from "react";
import uuid from 'react-uuid';
import Icon from "./Icon";
import Sprite from "./Sprite";
import SpriteCard from './SpriteCard';

export default function PreviewArea({sprites, addSprite, updateCurrentSelectedSprite, updateSpriteState, onSpriteClick, currentSelectedSprite, deleteSprite, updateDraggable, hideSprite}) {

  const parentRef   = useRef(null);
  const [parentHeight,setParentHeight] = useState(0);
  const [parentWidth,setParentWidth] = useState(0);
  const [parentY,setParentY] = useState(0);
  const [parentX,setParentX] = useState(0);
  
  const getSpriteState = (index) => {
    return sprites[index];
  };
  
  const sleep = (time) => {
    return new Promise(resolve => setTimeout(resolve, time));
  }

  const updateInputValue = (event,updatedItem) => {

    const currentSpriteState = sprites[currentSelectedSprite];
    currentSpriteState[updatedItem] = event.target.value;
    if(updatedItem === 'angle') {
      currentSpriteState[updatedItem] = currentSpriteState[updatedItem]%360;
    }
    updateSpriteState(currentSpriteState,currentSelectedSprite);

  }

  const getSpriteStyle = (sprite,index) => {

    const x = parseFloat(sprite.x);
    const y = parseFloat(sprite.y);
    const height = parseFloat(sprite.height);
    const width = parseFloat(sprite.width);
    
    const xPosition = parentWidth/2 + x-(width/2);
    const yPosition = parentHeight/2 - y-(height/2) - width*index;
    const rotation = sprite.angle - 90;
    return { transform:`translate(${xPosition}px,${yPosition}px) rotateZ(${rotation}deg) `, width:'100px' }
  }

  const handleDragStart = (event,index) => {

    setCurrentSprite(index); 

  }
  
  const handleDrag = async (event,index,stopDrag) => {
    const parentBottom = parentRef.current.offsetTop+parentHeight;
    if(event.clientX > parentRef.current.offsetLeft && parentBottom > event.clientY && event.clientY > parentRef.current.offsetTop) {
      const currentSprite = getSpriteState(index);
      currentSprite.x = event.clientX - parentX;
      currentSprite.y = -(event.clientY - parentY);
      await sleep(1000/512);
      updateSpriteState(currentSprite,index);
    } else {
      stopDrag();
    }
  
  }

  const setCurrentSprite = (index) => {
    updateCurrentSelectedSprite(index);
  }

  useEffect ( () => {
        
      if(parentRef.current){
          const parentHeight = parentRef.current.offsetHeight;
          const parentWidth  = parentRef.current.offsetWidth;
          const parentY = parentRef.current.offsetTop + parentHeight/2;
          const parentX = parentRef.current.offsetLeft + parentWidth/2;
          setParentHeight(parentHeight);
          setParentWidth(parentWidth);
          setParentY(parentY);
          setParentX(parentX); 
      }

    
  }, [parentRef]);
  return (
    <div className="grid grid-rows-3 w-full h-full">
      <div className="flex-none w-full h-600px p-2 static row-span-2 mr-2 z-0" ref={parentRef}>
        {
          sprites.map((sprite,index) => (
            <Sprite 
              key={uuid()}
              index={index}
              sprite={sprite}
              setCurrentSprite={setCurrentSprite}
              getSpriteStyle={getSpriteStyle}
              onSpriteClick={onSpriteClick}
              handleDrag={handleDrag}
              updateDraggable={updateDraggable}
            />
          ))
        }
      </div>
      <div className="flex-none w-full pt-2 bg-blue-100 z-40">
        <div className="grid grid-rows-2 w-full h-1/4 bg-white pt-1">
          <div className="grid grid-cols-3  w-full h-full">
            <div className="grid grid-cols-2">
              <div className="m-auto">Sprite:</div> 
              <input className="w-20 bg-blue-100 rounded border-2 border-gray-300 p-1 h-8" type="text" onChange={(event) => {updateInputValue(event,'name')}} value={sprites[currentSelectedSprite].name}></input>
            </div>
            <div className="grid grid-cols-2">
              <div className="m-auto">X:</div> 
              <input className="w-20 bg-blue-100 rounded border-2 border-gray-300 p-1 h-8" type="text" onChange={(event) => {updateInputValue(event,'x')}} value={sprites[currentSelectedSprite].x}></input>
            </div>
            <div className="grid grid-cols-2">
              <div className="m-auto">Y:</div> 
              <input className="w-20 bg-blue-100 rounded border-2 border-gray-300 p-1 h-8" type="text" onChange={(event) => {updateInputValue(event,'y')}} value={sprites[currentSelectedSprite].y}></input>
            </div>
          </div>
          <div className="grid grid-cols-3  w-full h-full">
            <div>
              <div className="grid grid-cols-4">
                <button onClick={() => {hideSprite(false, currentSelectedSprite)}} className="col-start-2 m-auto border-2 border-gray-400 w-8 h-8 rounded"><Icon name="eye"  className={`mx-auto ${sprites[currentSelectedSprite].hide===true?`text-gray-200`:`text-blue-600`}`}/></button> 
                <button onClick={() => {hideSprite(true, currentSelectedSprite)}} className="col-start-3 m-auto border-2 border-gray-400 w-8 h-8 rounded"><Icon name="eye-slash" className={`mx-auto ${sprites[currentSelectedSprite].hide===true?`text-blue-500`:`text-gray-300`}`}/></button> 
              </div>
            </div>
            <div className="grid grid-cols-2">
              <div className="m-auto">Size:</div> 
              <input className="w-20 bg-blue-100 rounded border-2 border-gray-300 p-1 h-8" type="text" onChange={(event) => {updateInputValue(event,'height')}} value={sprites[currentSelectedSprite].height}></input>
            </div>
            <div className="grid grid-cols-2">
              <div className="m-auto">Angle:</div> 
              <input className="w-20 bg-blue-100 rounded border-2 border-gray-300 p-1 h-8" type="text" onChange={(event) => {updateInputValue(event,'angle')}} value={sprites[currentSelectedSprite].angle}></input>
            </div>
          </div>
        </div>
        <div className="grid grid-cols-5">
          <div onClick={addSprite} className="bg-green-400 w-100px rounded p-1 pl-2 mt-2 m-auto col-start-5 cursor-pointer">
            ADD SPRITE
          </div>
        </div>
        <div className="flex flex-row flex-wrap justify-start overflow-auto w-full h-200px pt-2 bg-blue-100 z-40 ">
      {
          sprites.map((sprite,index) => (
            <SpriteCard
              key={uuid()} 
              setSelectedSprite={() => {setCurrentSprite(index)}}
              spriteName={sprite.name}
              selected={index===currentSelectedSprite}
              deleteSprite={() =>{deleteSprite(index)}}
            />
          ))
      }
      </div>
      </div>
      
    </div>
  );
}
