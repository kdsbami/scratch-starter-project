import React from "react";
import contentHandler from '../utility/contentHandler';

export default function ControlBlock({ contentArray, callbackAction,getSpriteState,updateSpriteState,currentSelectedSprite, updateDynamicArguments, executableId, nodeId, origin, contentIndex, addToControlChain }) {

  const dragOver = (event) => {
    event.preventDefault();
  }
  const onClickSideBar = () =>{callbackAction['action'](...callbackAction.dynamicArguments,...callbackAction.staticArguments,currentSelectedSprite ,getSpriteState,updateSpriteState)}
  const onClickMidarea = () =>{}
  const classnameCustom = origin === 'sidebar'? 'rounded':'';
  return (
    (
    <div onClick={origin==='sidebar'?onClickSideBar:onClickMidarea} className={`flex flex-row flex-wrap w-max bg-yellow-400 text-white px-2 py-1 text-sm cursor-pointer ${classnameCustom}`}>
        <div className="flex flex-row flex-wrap">
        {
          !contentArray['customStyle'] &&
            contentArray['content'].map((contentItem,index) => (
              <div key={contentItem.type+index}>
                {
                  contentHandler[contentItem.type]
                  (
                    ...contentItem.arguments,
                    (event) => 
                      {
                        origin === 'sidebar'?
                        updateDynamicArguments(event, contentItem.index, contentIndex,'control'):
                        updateDynamicArguments(event,contentItem.index,executableId,nodeId)
                      },
                    callbackAction.dynamicArguments[contentItem.index],
                    contentItem.type+index
                  )
                }
              </div>
            ))
        }
        {
          contentArray['customStyle'] &&
          contentArray['content'].map((contentItem,index) => (
            <div key={contentItem.type+index}>
              {
                contentHandler[contentItem.type]
                (
                  ...contentItem.arguments,
                  (event) => 
                    {
                      origin === 'sidebar'?
                      updateDynamicArguments(event, contentItem.index, contentIndex,'control'):
                      updateDynamicArguments(event,contentItem.index,executableId,nodeId)
                    },
                  callbackAction.dynamicArguments[contentItem.index],
                  contentItem.type+index
                )
              }
            </div>
          ))
        }
        </div>
        {
          contentArray['innerLayers'].map((name,layerIndex) => (
            <div>
              {name}
              <div onDragOver={dragOver} onDrop={(event) => addToControlChain(event,executableId,nodeId,layerIndex)} className='h-30px bg-white mt-1'></div>
            </div>
          ))
        }
    </div>)
  );
}
