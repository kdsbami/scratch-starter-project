import React from "react";
import Icon from "./Icon";
import uuid from 'react-uuid'

export default function SpriteCard({ spriteName,setSelectedSprite,selected,deleteSprite }) {

  const defaultClassName="grid grid-cols-3 cursor-pointer h-30px w-100px rounded m-3 text-center py-auto "
  return (
    (
    <div className={defaultClassName+" "+(selected?"bg-blue-400":"bg-white")}>
      <div onClick={setSelectedSprite} className="col-span-2 overflow-hidden"> {spriteName}</div>
       {!selected && <div className="p-1" onClick={deleteSprite}><Icon name="trash-alt" size="20" className="text-gray-500 mx-2"/></div>}
    </div>)
  );
}
