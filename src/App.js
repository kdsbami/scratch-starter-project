import React, { useState, useEffect, useRef, useCallback } from "react";
import Sidebar from "./components/Sidebar";
import MidArea from "./components/MidArea";
import PreviewArea from "./components/PreviewArea";
import Icon from "./components/Icon";
import 'react-tippy/dist/tippy.css';

export default function App() {

  const useStateWithPromise = (initialState) => {
    const [state, setState] = useState(initialState);
    const resolverRef = useRef(null);
  
    useEffect(() => {
      if (resolverRef.current) {
        resolverRef.current(state);
        resolverRef.current = null;
      }
    }, [resolverRef.current, state]);
  
    const handleSetState = useCallback((stateAction) => {
      setState(stateAction);
      return new Promise(resolve => {
        resolverRef.current = resolve;
      });
    }, [setState])
  
    return [state, handleSetState];
  };
  const [sprites,setSprites] = useStateWithPromise([{x:0,y:0,angle:90,height:100,width:100,name:'Sprite',hide:false, isDraggable:false, interactionContent:'',interactionType:0}]);
  const [currentSelectedSprite,setCurrentSelectedSprite] = useStateWithPromise(0);
  const [spriteExecutables, setSpriteExecutables] = useState([[]]);
  const [spriteExecutions, setSpriteExecutions] = useStateWithPromise([]);
  const [flagState, setFlagState] = useState({});
  const [keyPressState, setKeyPressState] = useState({});
  const [spriteClickState, setSpriteClickState] = useState({});
  
  const getSpriteState = (index) => {

    return sprites[index];

  };

  const updateSpriteState = (updatedSprite,index) => {
    const newSprites = [

      ...sprites.slice(0,index),
      updatedSprite,
      ...sprites.slice(index+1)

    ];
    return setSprites(newSprites);
  }

  const updateDraggable = (value,index)=> {
    const newSprite = sprites[index];
    newSprite.isDraggable = value;
    updateSpriteState(newSprite,index);
  }

  const hideSprite = (value, index) => {
    const newSprite = sprites[index];
    newSprite.hide = value;
    updateSpriteState(newSprite,index);
  }

  const compile = (spriteId) => {

    const spriteExecutable = spriteExecutables[spriteId];
    const flagArray = [];
    const spriteClicked = [];
    const keyPress = {};
    const spriteExecutionsArray = [];
    executablesLoop: for(const executableId in spriteExecutable) {
      const executableChain = spriteExecutable[executableId].executableChain;
      const currentSpriteExecutable = spriteExecutable[executableId];
      const head = executableChain[0];
      
      const dynamicCallback =async ()=> {
        for(let i=0;i<executableChain.length;i++) {
          const actionArguments = executableChain[i]['callbackAction'].dynamicArguments;
          const staticArguments = executableChain[i]['callbackAction'].staticArguments;
          if(executableChain[i].blockType === 'control') {
            const dynamicCallbacks = [];
            const position = i;
            const limit = executableChain[i]['callbackAction'].callbackCount;
            callbackLoop:for(let cBackItr =0;cBackItr<limit;cBackItr++) {
              i++;
              if(executableChain.length<=i){
                break;
              };
              const innerActionArguments = executableChain[i]['callbackAction'].dynamicArguments;
              const innerStaticArguments = executableChain[i]['callbackAction'].staticArguments;
              const fn = () => executableChain[i]['callbackAction'].action(...innerActionArguments,...innerStaticArguments,spriteId,getSpriteState,updateSpriteState)
              dynamicCallbacks.push(fn);
            } 
            if(executableChain.length<=i) {
              await new executableChain[position]['callbackAction'].action(
                ...actionArguments,
                ...staticArguments,
                spriteId,
                getSpriteState,
                updateSpriteState
              );
            } else {
              await executableChain[position]['callbackAction'].action(
                ...(actionArguments.slice(0,actionArguments.length-limit)),
                ...dynamicCallbacks,
                ...staticArguments,
                spriteId,
                getSpriteState,
                updateSpriteState
              );
            }
              
          } else {

            await new executableChain[i]['callbackAction'].action(...actionArguments,...staticArguments,spriteId,getSpriteState,updateSpriteState);

          }
          
        }
      }
      if(head.eventId === 'flagClick') {
        flagArray.push(dynamicCallback);
      }
      else if(head.eventId === 'spriteClick') {
        spriteClicked.push(dynamicCallback);
      }
      else if(head.eventId === 'keyPress') {
        if (head['callbackAction'].dynamicArguments[0] in keyPress) {
          keyPress[head['callbackAction'].dynamicArguments[0]].push(dynamicCallback);
        } else {
          keyPress[head['callbackAction'].dynamicArguments[0]] = [];
          keyPress[head['callbackAction'].dynamicArguments[0]].push(dynamicCallback);
        }
      }
      spriteExecutionsArray[executableId] = dynamicCallback;
      const newSpriteExecutions = [
        ...spriteExecutions.slice(0,spriteId),
        spriteExecutionsArray,
        ...spriteExecutions.slice(spriteId+1)
      ]
      setSpriteExecutions([...newSpriteExecutions]);
    }
    const updatedFlagValue = {[spriteId]:flagArray};
    const updatedSpriteClickedValue = {[spriteId]:spriteClicked};
    const updatedKeyPressValue = {[spriteId]:keyPress};
    setFlagState((previousState) => ({
      ...previousState,
      ...updatedFlagValue
    }));
    setSpriteClickState((previousState) => ({
      ...previousState,
      ...updatedSpriteClickedValue
    }));
    setKeyPressState((previousState) => ({
      ...previousState,
      ...updatedKeyPressValue
    }));
  }

  const onFlagClickCallback = () => {
    const spriteChain = flagState;
    spriteLoop: for(const spriteId in spriteChain) {
      const spriteExecutable = spriteChain[spriteId];
      if(spriteExecutable.length <= 0) continue spriteLoop;
      executableChainLoop: for(const executableId in spriteExecutable) {
        const callback = spriteExecutable[executableId];
        callback();
      }
    }
  }

  const onSpriteClick = (spriteId) => {
    const spriteExecutable = spriteClickState[spriteId];
      executableChainLoop:for(const executableId in spriteExecutable) {
        const callback = spriteExecutable[executableId];
        callback();
      }
  }

  const onKeyPress = (event) => {
    const spriteChain = keyPressState;
    
    spriteLoop: for(const spriteId in spriteChain) {
      const spriteExecutable = spriteChain[spriteId];
      if(!(event.key in spriteExecutable)) continue spriteLoop;
      executableChainLoop: for(const executableId in spriteExecutable[event.key]) {
        const callback = spriteExecutable[event.key][executableId];
        callback();
      }
    }
  }

  const deleteSprite = async(index) => {

    if(sprites.length === 1) return;
    
    if(index < currentSelectedSprite) {
      await setCurrentSelectedSprite(currentSelectedSprite-1);
    }

    const newSprites = [
      ...sprites.slice(0,index),
      ...sprites.slice(index+1)
    ];
    const newExecutables = [
      ...spriteExecutables.slice(0,index),
      ...spriteExecutables.slice(index+1)
    ];
    
    setSpriteExecutables(newExecutables);
    await setSprites(newSprites);
    

  }

  const addSprite = () => {
    const newExecutables = [...spriteExecutables];
    newExecutables.push([]);
    setSpriteExecutables(newExecutables);
    const newSprites = [...sprites];
    const name = `Sprite`
    newSprites.push({x:0,y:0,angle:90,height:100,width:100,name:name,hide:false, interactionContent:'',interactionType:0});
    setSprites(newSprites)
  }
  
  return (
    <>
    <style>{
      `input[type=number]::-webkit-inner-spin-button,
      input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none;
      }`
    }</style>
    <div tabIndex="0" onKeyDown={onKeyPress} className="bg-blue-100 pt-6 font-sans">
      <div className="grid  grid-cols-3">
        <div className="col-start-3 col-span-1 flex flex-column">
            <div onClick={onFlagClickCallback}><Icon name="flag"  size={30} className="text-green-600"/></div>
        </div>
      </div>
      <div className="h-screen overflow-hidden flex flex-row  ">
        <div className="flex-1 h-screen overflow-hidden flex flex-row bg-white border-t border-r border-gray-200 rounded-tr-xl mr-2">
          <Sidebar 
            currentSelectedSprite={currentSelectedSprite}
            getSpriteState={getSpriteState}
            updateSpriteState={updateSpriteState} 
          />
          <MidArea
            spriteId={currentSelectedSprite}
            setExecutables={setSpriteExecutables}
            executables={spriteExecutables}
            spriteExecutions={spriteExecutions}
            getSpriteState={getSpriteState}
            updateSpriteState={updateSpriteState}
            compiler={compile}
          />
        </div>
        <div className="w-1/3 h-screen overflow-hidden flex flex-row bg-white border-t border-l border-gray-200 rounded-xl mx-2">
          <PreviewArea 
            sprites={sprites}
            setSprites={setSprites}
            currentSelectedSprite={currentSelectedSprite}
            updateCurrentSelectedSprite={setCurrentSelectedSprite}
            getSpriteState={getSpriteState}
            updateSpriteState={updateSpriteState}
            onSpriteClick={onSpriteClick}
            deleteSprite={deleteSprite}
            updateDraggable={updateDraggable}
            addSprite={addSprite}
            hideSprite={hideSprite}
          />
        </div>
      </div>
    </div>
    </>
  );
}
