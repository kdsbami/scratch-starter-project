import React from 'react';
import Icon from "../components/Icon";

const typeString = (string) => {
    return (`${string.toLowerCase()} `);
}

const typeIcon = (icon, className='text-green-600 mx-2',size=15) => {
    return (<Icon name={icon} size={size} className={className} />);
}

const typeInput = (inputCallback,value) => {
    return (<input type="Number" value={value} onClick={(event)=> {event.stopPropagation()}} onChange={inputCallback} className="w-8 rounded text-black mx-2 px-1" />);
}

const typeInputText = (inputCallback,value) => {
    return (<input type="text" value={value} onClick={(event)=> {event.stopPropagation()}} onChange={inputCallback} className="w-16 rounded text-black mx-2 px-1" />);
}

const typeSelect = (values, inputCallback,defaultValue) => {
    return (
    <select defaultValue={defaultValue} onClick={(event)=> {event.stopPropagation()}} onChange={inputCallback} className="w-18 rounded text-black mx-2 px-1" >
        {
            values.map(value => (
                <option key={value} value={value} >{value}</option>
            ))
        }
    </select>
    );
}

const contentHandler = {

    'string':typeString,
    'icon':typeIcon,
    'input':typeInput,
    'select':typeSelect,
    'inputText':typeInputText

}

export default contentHandler;