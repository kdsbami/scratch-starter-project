const blockType = 'event';
import keys from '../utility/keys'
const eventFunctionality = [

    {
        blockType:blockType,
        canHaveHead:false,
        canHaveTail:true,
        callbackAction:{action:() =>{},dynamicArguments:[],staticArguments:[]}, 
        contentArray:[{type:'string', arguments:['When']},{type:'icon', arguments:['flag','text-green-600 mx-2',15]},{type:'string', arguments:['clicked']}],
        eventId:'flagClick'
    },
    {
        blockType:blockType,
        canHaveHead:false,
        canHaveTail:true,
        callbackAction:{action:() =>{},dynamicArguments:[],staticArguments:[]}, 
        contentArray:[{type:'string', arguments:['When this sprite is clicked']}],
        eventId:'spriteClick'
    },
    {
        blockType:blockType,
        canHaveHead:false,
        canHaveTail:true,
        callbackAction:{action:() =>{},dynamicArguments:['0'],staticArguments:[]}, 
        contentArray:[{type:'string', arguments:['When']},{type:'select', arguments:[Object.keys(keys)], index:0},{type:'string', arguments:['key is pressed']}],
        eventId:'keyPress'
    }

];

export default eventFunctionality;