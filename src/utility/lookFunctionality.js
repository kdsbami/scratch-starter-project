const blockType = 'look';

const sleep = (time) => {
    return new Promise(resolve => setTimeout(resolve, time))
  }

const wait = async (waitTime,callback)=> {
    await sleep(waitTime);
    return callback();
}

const speakForNSeconds = async(content, time,index,getSpriteState,updateSpriteStates) => {
    const currentSpriteState = getSpriteState(index);
    currentSpriteState.interactionContent = content;
    await updateSpriteStates(currentSpriteState,index);
    return wait(time*1000,()=>{
        const currentSpriteStateBack = getSpriteState(index);
        currentSpriteStateBack.interactionContent = '';
        return updateSpriteStates(currentSpriteState,index);
    })
}

const changeSize = (size,index,getSpriteState,updateSpriteStates) => {
    const currentSpriteState = getSpriteState(index);
    const sizeValue = (parseInt(currentSpriteState.height) + parseInt(size));
    currentSpriteState.height = sizeValue<0?0:sizeValue;
    return updateSpriteStates(currentSpriteState,index);
}

const changeSizePercentage = (size,index,getSpriteState,updateSpriteStates) => {
    const currentSpriteState = getSpriteState(index);
    currentSpriteState.height = (100*(size/100)) <0?0:(100*(size/100)) ;
    return updateSpriteStates(currentSpriteState,index);
}

const show = (index,getSpriteState,updateSpriteStates) => {
    const currentSpriteState = getSpriteState(index);
    currentSpriteState.hide = false ;
    return updateSpriteStates(currentSpriteState,index);
}

const hide = (index,getSpriteState,updateSpriteStates) => {
    const currentSpriteState = getSpriteState(index);
    currentSpriteState.hide = true ;
    return updateSpriteStates(currentSpriteState,index);
}
const lookFunctionality = [
    {
        blockType:blockType,
        canHaveHead:true,
        canHaveTail:true,
        callbackAction:{action:speakForNSeconds,dynamicArguments:['hello!',2],staticArguments:[]}, 
        contentArray:[{type:'string', arguments:[`say`]},{type:'inputText',arguments:[], index:0},{type:'string', arguments:[' for']},{type:'input',arguments:[], index:1},{type:'string',arguments:['seconds']}]
    },
    {
        blockType:blockType,
        canHaveHead:true,
        canHaveTail:true,
        callbackAction:{action:changeSize,dynamicArguments:[10],staticArguments:[]}, 
        contentArray:[{type:'string', arguments:[`change size by`]},{type:'input',arguments:[], index:0}]
    },
    {
        blockType:blockType,
        canHaveHead:true,
        canHaveTail:true,
        callbackAction:{action:changeSizePercentage,dynamicArguments:[100],staticArguments:[]}, 
        contentArray:[{type:'string', arguments:[`set size to`]},{type:'input',arguments:[], index:0},{type:'string', arguments:[`%`]}]
    },
    {
        blockType:blockType,
        canHaveHead:true,
        canHaveTail:true,
        callbackAction:{action:show,dynamicArguments:[],staticArguments:[]}, 
        contentArray:[{type:'string', arguments:[`Show`]}]
    },
    {
        blockType:blockType,
        canHaveHead:true,
        canHaveTail:true,
        callbackAction:{action:hide,dynamicArguments:[],staticArguments:[]}, 
        contentArray:[{type:'string', arguments:[`Hide`]}]
    }
];

export default lookFunctionality;
