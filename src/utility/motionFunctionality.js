const blockType = 'motion';

const onMovementClick = (steps,index,getSpriteState, updateSpriteStates) => {
    const currentSpriteState = getSpriteState(index);
    const currentX = parseInt(currentSpriteState.x);
    const currentY = parseInt(currentSpriteState.y);
    const currentRotation = parseInt(currentSpriteState.angle) - 90;
    const updatedX = currentX + parseInt(steps)* Math.cos((Math.PI * currentRotation / 180));
    const updatedY = currentY - parseInt(steps)* Math.sin((Math.PI * currentRotation / 180));
    currentSpriteState.x = updatedX;
    currentSpriteState.y = updatedY;
    return updateSpriteStates(currentSpriteState,index);
}
  
const onRotationClick = (degrees, direction,index,getSpriteState,updateSpriteStates) => {
    const intDegrees = parseInt(degrees);
    const currentSpriteState = getSpriteState(index);
    const currentRotation = parseInt(currentSpriteState.angle);
    currentSpriteState.angle = direction === 'clockwise'?currentRotation+intDegrees:currentRotation-intDegrees;
    currentSpriteState.angle = (currentSpriteState.angle)%360;
    return updateSpriteStates(currentSpriteState,index);
}

const setXY = (x, y,index,getSpriteState,updateSpriteStates) => {
    const currentSpriteState = getSpriteState(index);
    currentSpriteState.x = parseInt(x);
    currentSpriteState.y = parseInt(y);
    return updateSpriteStates(currentSpriteState,index);
}

const setAngle = (angle,index,getSpriteState,updateSpriteStates) => {
    const currentSpriteState = getSpriteState(index);
    currentSpriteState.angle = parseInt(angle);
    currentSpriteState.angle = (currentSpriteState.angle)%360;
    return updateSpriteStates(currentSpriteState,index);
}

const changeX = (X,index,getSpriteState,updateSpriteStates) => {
    const currentSpriteState = getSpriteState(index);
    currentSpriteState.x = parseInt(currentSpriteState.x) + parseInt(X);
    return updateSpriteStates(currentSpriteState,index);
}

const changeXTo = (X,index,getSpriteState,updateSpriteStates) => {
    const currentSpriteState = getSpriteState(index);
    currentSpriteState.x = parseInt(X);
    return updateSpriteStates(currentSpriteState,index);
}

const changeYTo = (Y,index,getSpriteState,updateSpriteStates) => {
    const currentSpriteState = getSpriteState(index);
    currentSpriteState.x = parseInt(Y);
    return updateSpriteStates(currentSpriteState,index);
}

const changeY = (Y,index,getSpriteState,updateSpriteStates) => {
    const currentSpriteState = getSpriteState(index);
    currentSpriteState.y = parseInt(currentSpriteState.y) + parseInt(Y);
    return updateSpriteStates(currentSpriteState,index);
}

const sleep = (time) => {
    return new Promise(resolve => setTimeout(resolve, time))
  }

const wait = async (waitTime,callback)=> {
    await sleep(waitTime);
    return callback();
}

const glidexy = async(time,x,y,index,getSpriteState,updateSpriteStates) => {
    const msTime = time*1000;
    const newX = x;
    const newY = y;        
    const initialSpriteState = getSpriteState(index);
    const oldX = parseFloat(initialSpriteState.x);
    const oldY = parseFloat(initialSpriteState.y);
    const distanceRatio = 1/30;
    const stepX = ((1-distanceRatio)*oldX + distanceRatio*newX)-oldX;
    const stepY = ((1-distanceRatio)*oldY + distanceRatio*newY)-oldY;
    for(let i=0;i<30;i++) {
        await wait((msTime/29),
        async () => {
            const currentSpriteState = getSpriteState(index);
            currentSpriteState.x = (parseFloat(currentSpriteState.x) + stepX);
            currentSpriteState.y = (parseFloat(currentSpriteState.y) + stepY);
            await updateSpriteStates(currentSpriteState,index);
        });
    }
    const updatedState = getSpriteState(index);
    updatedState.x = newX;
    updatedState.y = newY;
    return updateSpriteStates(updatedState,index);
}


const motionFunctionality = [

    {
        blockType:blockType,
        canHaveHead:true,
        canHaveTail:true,
        callbackAction:{action:onMovementClick,dynamicArguments:[10],staticArguments:[]}, 
        contentArray:[{type:'string', arguments:[`Move`]},{type:'input',arguments:[], index:0},{type:'string', arguments:[' steps']}]
    },
    {
        blockType:blockType,
        canHaveHead:true,
        canHaveTail:true,
        callbackAction:{action:onRotationClick,dynamicArguments:[15],staticArguments:['clockwise']},
        contentArray:[{type:'string', arguments:['Turn']},{type:'icon', arguments:['redo','text-white mx-2',15]},{type:'input',arguments:[], index:0},{type:'string', arguments:['degrees']}] 
    },
    {
        blockType:blockType,
        canHaveHead:true,
        canHaveTail:true,
        callbackAction:{action:onRotationClick,dynamicArguments:[15],staticArguments:['anticlockwise']},
        contentArray:[{type:'string', arguments:['Turn']},{type:'icon', arguments:['undo','text-white mx-2',15]},{type:'input',arguments:[], index:0},{type:'string', arguments:['degrees']}] 
    },
    {
        blockType:blockType,
        canHaveHead:true,
        canHaveTail:true,
        callbackAction:{action:setXY,dynamicArguments:[0,0],staticArguments:[]},
        contentArray:[{type:'string', arguments:['go to x:']},{type:'input',arguments:[], index:0},{type:'string', arguments:['y:']},{type:'input',arguments:[], index:1}] 
    },
    {
        blockType:blockType,
        canHaveHead:true,
        canHaveTail:true,
        callbackAction:{action:setAngle,dynamicArguments:[0],staticArguments:[]},
        contentArray:[{type:'string', arguments:['point in direction:']},{type:'input',arguments:[], index:0}] 
    },
    {
        blockType:blockType,
        canHaveHead:true,
        canHaveTail:true,
        callbackAction:{action:changeX,dynamicArguments:[0],staticArguments:[]},
        contentArray:[{type:'string', arguments:['Change X by:']},{type:'input',arguments:[], index:0}] 
    },
    {
        blockType:blockType,
        canHaveHead:true,
        canHaveTail:true,
        callbackAction:{action:changeY,dynamicArguments:[0],staticArguments:[]},
        contentArray:[{type:'string', arguments:['Change Y by:']},{type:'input',arguments:[], index:0}] 
    },
    {
        blockType:blockType,
        canHaveHead:true,
        canHaveTail:true,
        callbackAction:{action:changeXTo,dynamicArguments:[0],staticArguments:[]},
        contentArray:[{type:'string', arguments:['Change X to:']},{type:'input',arguments:[], index:0}] 
    },
    {
        blockType:blockType,
        canHaveHead:true,
        canHaveTail:true,
        callbackAction:{action:changeYTo,dynamicArguments:[0],staticArguments:[]},
        contentArray:[{type:'string', arguments:['Change Y to:']},{type:'input',arguments:[], index:0}] 
    },
    {
        blockType:blockType,
        canHaveHead:true,
        canHaveTail:true,
        callbackAction:{action:glidexy,dynamicArguments:[1,0,0],staticArguments:[]},
        contentArray:[{type:'string', arguments:['glide']},{type:'input',arguments:[], index:0},{type:'string', arguments:['secs to x:']},{type:'input',arguments:[], index:1},{type:'string', arguments:['y:']},{type:'input',arguments:[], index:2}] 
    }

];

export default motionFunctionality;
