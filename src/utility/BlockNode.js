export default class BlockNode {
    constructor(blockType, canHaveHead, canHaveTail, callbackAction, contentArray,eventId) {

        this.canHaveHead = canHaveHead;
        this.canHaveTail = canHaveTail;
        this.callbackAction = callbackAction;
        this.contentArray = contentArray;
        this.blockType = blockType;
        this.eventId = eventId;
    }
}