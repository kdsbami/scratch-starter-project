const blockType = 'control';
import keys from '../utility/keys'

const sleep = (time) => {
    return new Promise(resolve => setTimeout(resolve, time))
  }

const wait = async (waitTime,callback)=> {
    await sleep(waitTime*1000);
    return callback();
}

const controlFunctionality = [

    {
        blockType:blockType,
        canHaveHead:true,
        canHaveTail:true,
        callbackAction:{action:wait,dynamicArguments:[1,()=>{console.log('waited')}],callbackCount:1,staticArguments:[]}, 
        contentArray:{content:[{type:'string', arguments:[`Wait`]},{type:'input',arguments:[], index:0},{type:'string', arguments:[' seconds']}],customStyle:false,innerLayers:[],innerValues:[]},
    },
    // {
    //     blockType:blockType,
    //     canHaveHead:true,
    //     canHaveTail:true,
    //     callbackAction:{action:wait,dynamicArguments:[1,()=>{console.log('waited')}],callbackCount:1,staticArguments:[]}, 
    //     contentArray:{content:[{type:'string', arguments:[`Wait`]},{type:'input',arguments:[], index:0},{type:'string', arguments:[' seconds']}],customStyle:true,innerLayers:[''],innerValues:[0]},
    // }
    
    // compiler and renderer addition left 
];

export default controlFunctionality;